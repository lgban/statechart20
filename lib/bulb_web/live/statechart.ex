defmodule BulbWeb.Statechart do
  use GenServer
  
    def start_link(ui), do: GenServer.start_link(__MODULE__, ui)
  
    def init(ui) do
      :gproc.reg({:p, :l, :ui_event})
      {:ok, %{ui_pid: ui, st: :off}}
    end
  
    def handle_info(:switch, %{ui_pid: ui, st: :off} = state) do
      GenServer.cast(ui, :switch_on)
      {:noreply, state |> Map.put(:st, :on)}
    end

    def handle_info(:switch, %{ui_pid: ui, st: :on} = state) do
        GenServer.cast(ui, :switch_off)
        {:noreply, state |> Map.put(:st, :off)}
    end

    def handle_info(:alarm, %{ui_pid: ui, st: :off} = state) do
        timer = Process.send_after(self(), :tick, 1000)  # arm timer
        GenServer.cast(ui, :switch_on)
        {:noreply, state |> Map.put(:timer, timer) |> Map.put(:st, :alarm_on) }
    end

    def handle_info(:alarm, %{ui_pid: ui, st: :on} = state) do
        {:noreply, state}
    end
      
    def handle_info(:tick, %{ui_pid: ui, st: :alarm_on} = state) do
        timer = Process.send_after(self(), :tick, 1000)  # arm timer
        GenServer.cast(ui, :switch_off)
        {:noreply, state |> Map.put(:timer, timer) |> Map.put(:st, :alarm_off) }
    end

    def handle_info(:tick, %{ui_pid: ui, st: :alarm_off} = state) do
        timer = Process.send_after(self(), :tick, 1000)  # arm timer
        GenServer.cast(ui, :switch_on)
        {:noreply, state |> Map.put(:timer, timer) |> Map.put(:st, :alarm_on) }
    end
    
end